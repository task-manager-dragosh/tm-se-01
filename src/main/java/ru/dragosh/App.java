package ru.dragosh;

import java.util.ArrayList;
import java.util.Scanner;

public class App
{
    public static void main( String[] args ) {
        ArrayList<String> projects = new ArrayList<>();
        ArrayList<String> tasks = new ArrayList<>();

        System.out.println( "**** Project registration ****\n" +
                "Введите одну из команд:\n" +
                "show\n" +
                "add project (после завершения ввода названия/названий нажать Пробел + Ввод)\n" +
                "add task (после завершения ввода названия/названий нажать Пробел + Ввод)\n" +
                "remove project\n" +
                "remove task");
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();

        switch(string) {
            case "show": {
                show(projects);
                show(tasks);
                break;
            }

            case "add project": {
                addCollection(projects);
                break;
            }

            case "add task": {
                addCollection(tasks);
                break;
            }

            case "remove project": {
                removeCollection(projects);
                break;
            }

            case "remove task": {
                removeCollection(tasks);
                break;
            }
        }
        scanner.close();
    }
    static void addCollection(ArrayList<String> collection) {
        while (true) {
            Scanner scanner2 = new Scanner(System.in);
            String stringWhile = scanner2.nextLine();
            add(collection, stringWhile);
            show(collection);
            if(" ".equals(stringWhile)) {
                scanner2.close();

                break;
            }
        }
    }

    static void removeCollection(ArrayList<String> collection) {
        while (true) {
            Scanner scanner2 = new Scanner(System.in);
            String stringWhile = scanner2.nextLine();
            remove(collection, stringWhile);
            scanner2.close();
            break;
        }
    }

    static void add (ArrayList<String> arrayList, String name) {
        arrayList.add(name);
    }

    static void remove (ArrayList<String> arrayList, String name) {
        arrayList.remove(name);
    }

    static void show (ArrayList<String> arrayList) {
        for (String s : arrayList) {
            System.out.println(s);
        }
    }
}
